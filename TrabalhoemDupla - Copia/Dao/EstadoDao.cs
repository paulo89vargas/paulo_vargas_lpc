﻿using TrabalhoDuplaG2.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TrabalhoDuplaG2.Dao
{
    public class EstadoDao
    {
        List<Estado> datasource = new List<Estado>();
        private AppContexto db;

        public EstadoDao()
        {
            db = new AppContexto();            
        }

        public List<Estado> GetAll()
        {
     
            return db.Estados.ToList();

        }

        public Estado GetById(int id)
        {

            return db.Estados.Where(x => x.Id == id).FirstOrDefault();
        }               

        public Estado GetByNome(string uf)
        {
      
            return db.Estados.Where(x => x.Uf == uf).FirstOrDefault();
        }

        public void Adicionar(Estado estado)
        {

            db.Estados.Add(estado);
            db.SaveChanges();
        }

        public void Atualizar(Estado estado)
        {
            var obj = db.Estados.Where(x => x.Id == estado.Id).FirstOrDefault();
            if (obj != null)
            {                    
                    obj.Uf = estado.Uf;                    
                    obj.UfExtenso = estado.UfExtenso;
                    obj.Cidades = estado.Cidades;
                    db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Estados.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Estados.Remove(obj);
                db.SaveChanges();
            }
        }       

    }
}
