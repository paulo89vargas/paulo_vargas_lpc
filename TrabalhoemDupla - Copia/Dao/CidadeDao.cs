﻿using TrabalhoDuplaG2.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TrabalhoDuplaG2.Dao
{
    public class CidadeDao
    {
        List<Cidade> datasource = new List<Cidade>();
        private AppContexto db;

        public CidadeDao()
        {
            db = new AppContexto();            
        }

        public List<Cidade> GetAll()
        {
     
            return db.Cidades.ToList();

        }

        public Cidade GetById(int id)
        {

            return db.Cidades.Where(x => x.Id == id).FirstOrDefault();
        }               

        public Cidade GetByNome(string descricao)
        {
      
            return db.Cidades.Where(x => x.Descricao == descricao).FirstOrDefault();
        }

        public void Adicionar(Cidade cidade)
        {

            db.Cidades.Add(cidade);
            db.SaveChanges();
        }

        public void Atualizar(Cidade Cidade)
        {
            var obj = db.Cidades.Where(x => x.Id == Cidade.Id).FirstOrDefault();
            if (obj != null)
            {
                obj.Descricao = Cidade.Descricao;
                obj.Estado = Cidade.Estado;
               
                db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Cidades.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Cidades.Remove(obj);
                db.SaveChanges();
            }
        }       

    }
}
