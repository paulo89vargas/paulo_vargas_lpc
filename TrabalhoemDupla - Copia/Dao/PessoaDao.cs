﻿using TrabalhoDuplaG2.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TrabalhoDuplaG2.Dao
{
    public class PessoaDao
    {
        List<Pessoa> datasource = new List<Pessoa>();
        private AppContexto db;

        public PessoaDao()
        {
            db = new AppContexto();            
        }

        public List<Pessoa> GetAll()
        {
     
            return db.Pessoas.ToList();

        }

        public Pessoa GetById(int id)
        {

            return db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
        }               

        public Pessoa GetByNome(string nome)
        {
      
            return db.Pessoas.Where(x => x.Nome == nome).FirstOrDefault();
        }

        public void Adicionar(Pessoa pessoa)
        {

            db.Pessoas.Add(pessoa);
            db.SaveChanges();
        }

        public void Atualizar(Pessoa pessoa)
        {
            var obj = db.Pessoas.Where(x => x.Id == pessoa.Id).FirstOrDefault();
            if (obj != null)
            {                    
                    obj.Nome = pessoa.Nome;                    
                    obj.Peso = pessoa.Peso;
                    obj.DataNascimento = pessoa.DataNascimento;
                    db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Pessoas.Remove(obj);
                db.SaveChanges();
            }
        }       

    }
}
