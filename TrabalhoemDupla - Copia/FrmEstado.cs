﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoDuplaG2.Entidades;
using TrabalhoDuplaG2.Dao;
//using aula1603;

namespace TrabalhoDuplaG2
{
    public partial class FrmEstado : Form
    {
        private EstadoDao estadoDao;

        public FrmEstado()
        {
            InitializeComponent();
            estadoDao = new EstadoDao();
        }

        private void FrmEstado_Load(object sender, EventArgs e)
        {

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmEstadoPesquisa(estadoDao);
            formulario.ShowDialog();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            var estado = new Estado();

            estado.Uf = txtUf.Text;
            estado.UfExtenso = txtUfExtenso.Text;
            //estado.Cidades = txtCidade.Text;
            estadoDao.Adicionar(estado);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int cod = int.Parse(txtCodigo.Text);

            /*if(cod == null)
            {
                string nome = txtNomebusca.Text;
                var ob = pessoaDao.GetByNome(nome);

                if (ob != null)
                {

                    txtUf.Text = obj.Uf.ToString();
                    txtUfExtenso.Text = obj.UfExtenso.ToString();
                    txtCidade.Text = obj.Cidades.ToString();
                }
            }*/

            var obj = estadoDao.GetById(cod);

            if (obj != null)
            {
                txtUf.Text = obj.Uf.ToString();
                txtUfExtenso.Text = obj.UfExtenso.ToString();
                txtCidade.Text = obj.Cidades.ToString();
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var estado = new Estado();

            estado.Uf = txtUf.Text;
            estado.UfExtenso = txtUfExtenso.Text;
           // estado.Cidades = txtCidade.Text;
            estadoDao.Atualizar(estado);
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            estadoDao.Excluir(int.Parse(txtCodigo.Text));
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCodigo.Text = null;
            txtUf.Text = null;
            txtUfExtenso.Text = null;
            txtNomebusca.Text = null;
            txtCidade.Text = null;
        }
    }
}
