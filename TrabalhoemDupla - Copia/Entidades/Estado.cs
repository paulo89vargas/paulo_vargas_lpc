﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabalhoDuplaG2.Entidades
{

    public class Estado
    {
        public int Id { get; set; }
       
        public string Uf { get; set; }

        public string UfExtenso { get; set; }   
        
        public List<Cidade> Cidades { get; set; }

    }
}
