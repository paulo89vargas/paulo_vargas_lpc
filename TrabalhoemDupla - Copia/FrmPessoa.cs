﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoDuplaG2.Entidades;
using TrabalhoDuplaG2.Dao;
//using aula1603;

namespace TrabalhoDuplaG2
{
    public partial class Form1 : Form
    {        
        private PessoaDao pessoaDao;

        public Form1()
        {
            InitializeComponent();
            pessoaDao = new PessoaDao();
            PopularCombobox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void FrmPrincipal_FormClosing(object sender, FormClosedEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
        private void PopularCombobox()
        {
            //cmbExemplo.DataSource = pessoaDao.GetAll();
            //cmbExemplo.DisplayMember = "Nome";
            //cmbExemplo.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show(cmbExemplo.SelectedValue.ToString());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmPessoaPesquisa(pessoaDao);
            formulario.ShowDialog();
        }

        private void salvar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();
            
            pessoa.Nome = txtNome.Text;
            pessoa.Peso = float.Parse(txtPeso.Text);
            pessoa.DataNascimento = DateTime.Parse(txtDtaNascimento.Text);
            pessoaDao.Adicionar(pessoa);
        }

        private void resultdtanasc_TextChanged(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {            
            int cod = int.Parse(txtCodigo.Text);

            /*if(cod == null)
            {
                string nome = txtNomebusca.Text;
                var ob = pessoaDao.GetByNome(nome);

                if (ob != null)
                {

                    txtCPF.Text = ob.Cpf.ToString();
                    txtRG.Text = ob.Rg.ToString();
                    txtNome.Text = ob.Nome.ToString();
                    txtIdade.Text = ob.Idade.ToString();
                }
            }*/

            var obj = pessoaDao.GetById(cod);

            if (obj != null)
            {        
                txtNome.Text = obj.Nome.ToString();                
                txtPeso.Text = obj.Peso.ToString();
                txtDtaNascimento.Text = obj.DataNascimento.ToString();
            }       


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void nomepesquisa_Click(object sender, EventArgs e)
        {

        }

        private void editar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();
            
            pessoa.Nome = txtNome.Text;
            pessoa.Peso = float.Parse(txtPeso.Text);
            pessoa.DataNascimento = DateTime.Parse(txtDtaNascimento.Text);
            pessoaDao.Atualizar(pessoa);
        }

        private void excluir_Click(object sender, EventArgs e)
        {
            pessoaDao.Excluir(int.Parse(txtCodigo.Text));

        }

        private void textSemestre_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCodigo.Text = null;
            txtDtaNascimento.Text = null;
            txtNome.Text = null;
            txtNomebusca.Text = null;
            txtPeso.Text = null;
        }
    }
}
