﻿namespace TrabalhoDuplaG2
{
    partial class FrmEstado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpPesquisar = new System.Windows.Forms.GroupBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtNomebusca = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.grpCadastrar = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.txtUfExtenso = new System.Windows.Forms.TextBox();
            this.txtUf = new System.Windows.Forms.TextBox();
            this.lblUfExtenso = new System.Windows.Forms.Label();
            this.lblUf = new System.Windows.Forms.Label();
            this.grpPesquisar.SuspendLayout();
            this.grpCadastrar.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPesquisar
            // 
            this.grpPesquisar.Controls.Add(this.btnPesquisar);
            this.grpPesquisar.Controls.Add(this.lblCodigo);
            this.grpPesquisar.Controls.Add(this.txtCodigo);
            this.grpPesquisar.Controls.Add(this.txtNomebusca);
            this.grpPesquisar.Controls.Add(this.btnOk);
            this.grpPesquisar.Controls.Add(this.btnLimpar);
            this.grpPesquisar.Location = new System.Drawing.Point(9, 12);
            this.grpPesquisar.Name = "grpPesquisar";
            this.grpPesquisar.Size = new System.Drawing.Size(676, 85);
            this.grpPesquisar.TabIndex = 19;
            this.grpPesquisar.TabStop = false;
            this.grpPesquisar.Text = "Pesquisar";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(558, 38);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 8;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(50, 43);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(40, 13);
            this.lblCodigo.TabIndex = 3;
            this.lblCodigo.Text = "Código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(109, 40);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(68, 20);
            this.txtCodigo.TabIndex = 4;
            // 
            // txtNomebusca
            // 
            this.txtNomebusca.Location = new System.Drawing.Point(200, 40);
            this.txtNomebusca.Name = "txtNomebusca";
            this.txtNomebusca.Size = new System.Drawing.Size(163, 20);
            this.txtNomebusca.TabIndex = 5;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(396, 38);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(477, 38);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 7;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // grpCadastrar
            // 
            this.grpCadastrar.Controls.Add(this.txtCidade);
            this.grpCadastrar.Controls.Add(this.lblCidade);
            this.grpCadastrar.Controls.Add(this.btnSalvar);
            this.grpCadastrar.Controls.Add(this.btnEditar);
            this.grpCadastrar.Controls.Add(this.btnExcluir);
            this.grpCadastrar.Controls.Add(this.txtUfExtenso);
            this.grpCadastrar.Controls.Add(this.txtUf);
            this.grpCadastrar.Controls.Add(this.lblUfExtenso);
            this.grpCadastrar.Controls.Add(this.lblUf);
            this.grpCadastrar.Location = new System.Drawing.Point(12, 112);
            this.grpCadastrar.Name = "grpCadastrar";
            this.grpCadastrar.Size = new System.Drawing.Size(672, 197);
            this.grpCadastrar.TabIndex = 20;
            this.grpCadastrar.TabStop = false;
            this.grpCadastrar.Text = "Cadastrar";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(228, 110);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(311, 20);
            this.txtCidade.TabIndex = 19;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Location = new System.Drawing.Point(125, 113);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(40, 13);
            this.lblCidade.TabIndex = 18;
            this.lblCidade.Text = "Cidade";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(464, 155);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 17;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(288, 155);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 16;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(118, 155);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 15;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // txtUfExtenso
            // 
            this.txtUfExtenso.Location = new System.Drawing.Point(228, 68);
            this.txtUfExtenso.Name = "txtUfExtenso";
            this.txtUfExtenso.Size = new System.Drawing.Size(311, 20);
            this.txtUfExtenso.TabIndex = 14;
            // 
            // txtUf
            // 
            this.txtUf.Location = new System.Drawing.Point(228, 28);
            this.txtUf.Name = "txtUf";
            this.txtUf.Size = new System.Drawing.Size(311, 20);
            this.txtUf.TabIndex = 12;
            // 
            // lblUfExtenso
            // 
            this.lblUfExtenso.AutoSize = true;
            this.lblUfExtenso.Location = new System.Drawing.Point(125, 71);
            this.lblUfExtenso.Name = "lblUfExtenso";
            this.lblUfExtenso.Size = new System.Drawing.Size(45, 13);
            this.lblUfExtenso.TabIndex = 11;
            this.lblUfExtenso.Text = "Extenso";
            // 
            // lblUf
            // 
            this.lblUf.AutoSize = true;
            this.lblUf.Location = new System.Drawing.Point(125, 31);
            this.lblUf.Name = "lblUf";
            this.lblUf.Size = new System.Drawing.Size(18, 13);
            this.lblUf.TabIndex = 9;
            this.lblUf.Text = "Uf";
            // 
            // FrmEstado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 450);
            this.Controls.Add(this.grpCadastrar);
            this.Controls.Add(this.grpPesquisar);
            this.Name = "FrmEstado";
            this.Text = "Cadastro de Estado";
            this.Load += new System.EventHandler(this.FrmEstado_Load);
            this.grpPesquisar.ResumeLayout(false);
            this.grpPesquisar.PerformLayout();
            this.grpCadastrar.ResumeLayout(false);
            this.grpCadastrar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpPesquisar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtNomebusca;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox grpCadastrar;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.TextBox txtUfExtenso;
        private System.Windows.Forms.TextBox txtUf;
        private System.Windows.Forms.Label lblUfExtenso;
        private System.Windows.Forms.Label lblUf;
    }
}