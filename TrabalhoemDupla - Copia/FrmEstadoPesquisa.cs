﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoDuplaG2.Dao;

namespace TrabalhoDuplaG2
{
    public partial class FrmEstadoPesquisa : Form
    {
        private EstadoDao dao;

        public FrmEstadoPesquisa(EstadoDao dao)
        {
            InitializeComponent();
            this.dao = dao;
        }

        private void FrmEstadoPesquisa_Load(object sender, EventArgs e)
        {

        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            dgvPesquisa.DataSource = dao.GetAll();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
            //dgvPesquisa.DataSource = dao.GetByNome();
        }

        private void dgvPesquisa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvPesquisa.DataSource = dao.GetAll();
        }
    }
}
