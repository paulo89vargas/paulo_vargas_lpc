﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabalhoDuplaG2.Entidades;
using TrabalhoDuplaG2.Dao;
//using aula1603;

namespace TrabalhoDuplaG2
{
    public partial class FrmCidade : Form
    {
        private CidadeDao cidadeDao;

        public FrmCidade()
        {
            InitializeComponent();
            cidadeDao = new CidadeDao();
            
        }

        private void FrmCidade_Load(object sender, EventArgs e)
        {

        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmCidadePesquisa(cidadeDao);
            formulario.ShowDialog();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            var cidade = new Cidade();

            cidade.Descricao = txtDescricao.Text;
            cidade.EstadoId = int.Parse(txtEstado.Text);
            
            cidadeDao.Adicionar(cidade);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int cod = int.Parse(txtCodigo.Text);

            /*if(cod == null)
            {
                string nome = txtNomebusca.Text;
                var ob = pessoaDao.GetByNome(nome);

                if (ob != null)
                {

                    txtCPF.Text = ob.Cpf.ToString();
                    txtRG.Text = ob.Rg.ToString();
                    txtNome.Text = ob.Nome.ToString();
                    txtIdade.Text = ob.Idade.ToString();
                }
            }*/

            var obj = cidadeDao.GetById(cod);

            if (obj != null)
            {
                txtDescricao.Text = obj.Descricao.ToString();
                txtEstado.Text = obj.Estado.ToString();
                
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var cidade = new Cidade();

            cidade.Descricao = txtDescricao.Text;
            cidade.EstadoId = int.Parse(txtEstado.Text);
            cidadeDao.Atualizar(cidade);
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            cidadeDao.Excluir(int.Parse(txtCodigo.Text));
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCodigo.Text = null;           
            txtDescricao.Text = null;
            txtNomebusca.Text = null;
            txtEstado.Text = null;
        }
    }
}
