﻿namespace TrabalhoDuplaG2
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtNomebusca = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.grpPesquisar = new System.Windows.Forms.GroupBox();
            this.grpCadastrar = new System.Windows.Forms.GroupBox();
            this.txtDtaNascimento = new System.Windows.Forms.TextBox();
            this.lblDtaNascimento = new System.Windows.Forms.Label();
            this.grpPesquisar.SuspendLayout();
            this.grpCadastrar.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(50, 43);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(40, 13);
            this.lblCodigo.TabIndex = 3;
            this.lblCodigo.Text = "Código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(109, 40);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(68, 20);
            this.txtCodigo.TabIndex = 4;
            // 
            // txtNomebusca
            // 
            this.txtNomebusca.Location = new System.Drawing.Point(200, 40);
            this.txtNomebusca.Name = "txtNomebusca";
            this.txtNomebusca.Size = new System.Drawing.Size(163, 20);
            this.txtNomebusca.TabIndex = 5;
            this.txtNomebusca.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(396, 38);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.ok_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(477, 38);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 7;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(558, 38);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 8;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.pesquisar_Click);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(125, 31);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(82, 13);
            this.lblNome.TabIndex = 9;
            this.lblNome.Text = "Nome Completo";
            this.lblNome.Click += new System.EventHandler(this.nomepesquisa_Click);
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Location = new System.Drawing.Point(125, 71);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(31, 13);
            this.lblPeso.TabIndex = 11;
            this.lblPeso.Text = "Peso";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(228, 28);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(311, 20);
            this.txtNome.TabIndex = 12;
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(228, 68);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(311, 20);
            this.txtPeso.TabIndex = 14;
            this.txtPeso.TextChanged += new System.EventHandler(this.resultdtanasc_TextChanged);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(118, 155);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 15;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.excluir_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(288, 155);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 16;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.editar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(464, 155);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 17;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.salvar_Click);
            // 
            // grpPesquisar
            // 
            this.grpPesquisar.Controls.Add(this.btnPesquisar);
            this.grpPesquisar.Controls.Add(this.lblCodigo);
            this.grpPesquisar.Controls.Add(this.txtCodigo);
            this.grpPesquisar.Controls.Add(this.txtNomebusca);
            this.grpPesquisar.Controls.Add(this.btnOk);
            this.grpPesquisar.Controls.Add(this.btnLimpar);
            this.grpPesquisar.Location = new System.Drawing.Point(12, 10);
            this.grpPesquisar.Name = "grpPesquisar";
            this.grpPesquisar.Size = new System.Drawing.Size(676, 85);
            this.grpPesquisar.TabIndex = 18;
            this.grpPesquisar.TabStop = false;
            this.grpPesquisar.Text = "Pesquisar";
            this.grpPesquisar.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // grpCadastrar
            // 
            this.grpCadastrar.Controls.Add(this.txtDtaNascimento);
            this.grpCadastrar.Controls.Add(this.lblDtaNascimento);
            this.grpCadastrar.Controls.Add(this.btnSalvar);
            this.grpCadastrar.Controls.Add(this.btnEditar);
            this.grpCadastrar.Controls.Add(this.btnExcluir);
            this.grpCadastrar.Controls.Add(this.txtPeso);
            this.grpCadastrar.Controls.Add(this.txtNome);
            this.grpCadastrar.Controls.Add(this.lblPeso);
            this.grpCadastrar.Controls.Add(this.lblNome);
            this.grpCadastrar.Location = new System.Drawing.Point(12, 111);
            this.grpCadastrar.Name = "grpCadastrar";
            this.grpCadastrar.Size = new System.Drawing.Size(672, 197);
            this.grpCadastrar.TabIndex = 19;
            this.grpCadastrar.TabStop = false;
            this.grpCadastrar.Text = "Cadastrar";
            // 
            // txtDtaNascimento
            // 
            this.txtDtaNascimento.Location = new System.Drawing.Point(228, 110);
            this.txtDtaNascimento.Name = "txtDtaNascimento";
            this.txtDtaNascimento.Size = new System.Drawing.Size(311, 20);
            this.txtDtaNascimento.TabIndex = 19;
            // 
            // lblDtaNascimento
            // 
            this.lblDtaNascimento.AutoSize = true;
            this.lblDtaNascimento.Location = new System.Drawing.Point(125, 113);
            this.lblDtaNascimento.Name = "lblDtaNascimento";
            this.lblDtaNascimento.Size = new System.Drawing.Size(102, 13);
            this.lblDtaNascimento.TabIndex = 18;
            this.lblDtaNascimento.Text = "Data de nascimento";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 324);
            this.Controls.Add(this.grpCadastrar);
            this.Controls.Add(this.grpPesquisar);
            this.Name = "Form1";
            this.Text = "Cadastro de Pessoa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpPesquisar.ResumeLayout(false);
            this.grpPesquisar.PerformLayout();
            this.grpCadastrar.ResumeLayout(false);
            this.grpCadastrar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtNomebusca;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.GroupBox grpPesquisar;
        private System.Windows.Forms.GroupBox grpCadastrar;
        private System.Windows.Forms.TextBox txtDtaNascimento;
        private System.Windows.Forms.Label lblDtaNascimento;
    }
}

