﻿using System.Collections.Generic;
using System;

namespace Aula5.LinqConsole.Entidades
{
    public class Estado
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public float Peso { get; set; }

        public DateTime DataNascimento { get; set; }
    }
}
