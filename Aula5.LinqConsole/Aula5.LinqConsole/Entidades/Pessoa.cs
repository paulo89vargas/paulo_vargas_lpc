﻿using System;

namespace Aula5.LinqConsole.Entidades
{
    public class Pessoa
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public float Peso { get; set; }

        public DateTime DataNascimento { get; set; }
    }
}
