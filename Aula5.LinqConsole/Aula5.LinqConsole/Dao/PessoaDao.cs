﻿using Aula5.LinqConsole.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aula5.LinqConsole.Dao
{
    public class PessoaDao
    {
        List<Pessoa> datasource = new List<Pessoa>();

        public PessoaDao()
        {
            datasource = new List<Pessoa>();

            datasource.Add(new Pessoa { Id = 1, Nome = "Pedro", Peso = 80, DataNascimento = new DateTime(1985, 10, 3) });
            datasource.Add(new Pessoa { Id = 2, Nome = "Juca", Peso = 82, DataNascimento = new DateTime(1990, 4, 20) });
            datasource.Add(new Pessoa { Id = 3, Nome = "Maria", Peso = 60, DataNascimento = new DateTime(1992, 2, 1) });
            datasource.Add(new Pessoa { Id = 4, Nome = "Julia", Peso = 95, DataNascimento = new DateTime(1996, 1, 3) });
            datasource.Add(new Pessoa { Id = 5, Nome = "Everaldo", Peso = 110, DataNascimento = new DateTime(2000, 8, 20) });
            datasource.Add(new Pessoa { Id = 6, Nome = "Fatima", Peso = 85, DataNascimento = new DateTime(1978, 9, 15) });
            datasource.Add(new Pessoa { Id = 7, Nome = "Patricia", Peso = 66, DataNascimento = new DateTime(1964, 12, 25) });
            datasource.Add(new Pessoa { Id = 8, Nome = "Bernardo", Peso = 61, DataNascimento = new DateTime(1990, 1, 19) });
            datasource.Add(new Pessoa { Id = 9, Nome = "Ricardo", Peso = 56, DataNascimento = new DateTime(1996, 4, 9) });
            datasource.Add(new Pessoa { Id = 10, Nome = "Maicon", Peso = 45, DataNascimento = new DateTime(2002, 12, 3) });
        }

        public List<Pessoa> GetAll()
        {
            return datasource;
        }

        public List<Pessoa> GetByNome(string nome)
        {
            return datasource.Where(x => x.Nome == nome).ToList();
        }

        public Pessoa GetById(int id)
        {
            return datasource.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Adicionar(Pessoa pessoa)
        {
            datasource.Add(pessoa);
        }

        public void Atualizar(Pessoa pessoa)
        {
            var obj = datasource.Where(x => x.Id == pessoa.Id).FirstOrDefault();
            if (obj != null)
            {
                obj.Peso = pessoa.Peso;
                obj.Nome = pessoa.Nome;
                obj.DataNascimento = pessoa.DataNascimento;
            }
        }

        public void Excluir(int id)
        {
            var obj = datasource.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                datasource.Remove(obj);
            }
        }

        public List<Pessoa> GetByDatNascimento(DateTime data)
        {
            return datasource
                .Where(x => x.DataNascimento == data)
                .OrderBy(x => x.Nome)
                .ToList();
        }

        public List<Pessoa> GetByDatNascimento(DateTime dataInicial, DateTime dataFinal)
        {
            return datasource
                .Where(x => x.DataNascimento >= dataInicial && x.DataNascimento <= dataFinal)
                .OrderBy(x => x.Nome)
                .ToList();
        }

    }
}
