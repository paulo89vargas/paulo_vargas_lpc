﻿using trabalhoG2Dupla.Dao;
using trabalhoG2Dupla.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace trabalhoG2Dupla.Dao
{
    public class Dao
    {
        List<Estado> datasource = new List<Estado>();
        private AppContexto db;

        public Dao()
        {
            db = new AppContexto();

            datasource = new List<Estado>();

             }

        public List<Estado> GetAll()
        {
     
            return db.Estados.ToList();

        }

        public List<Estado> GetByUf(string UF)
        {
      
            return db.Estados.Where(x => x.Uf == UF).ToList();
        }

        public List<Estado> GetByUfExtenso(string extenso)
        {

            return db.Estados.Where(x => x.UfExtenso == extenso).ToList();
        }

        public Estado GetById(int id)
        {
      
            return db.Estados.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Adicionar(Estado estado)
        {

            db.Estados.Add(estado);
            db.SaveChanges();
        }

        public void Atualizar(Estado estado)
        {
            var obj = db.Estados.Where(x => x.Id == estado.Id).FirstOrDefault();
            if (obj != null)
            {
                obj.Id = estado.Id;
                obj.Uf = estado.Uf;
                obj.UfExtenso = estado.UfExtenso;

                db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Estados.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Estados.Remove(obj);
                db.SaveChanges();
            }
        }

        public List<Estado> GetByUF(String uf)
        {
            return db.Estados
                .Where(x => x.Uf == uf)
                .OrderBy(x => x.Uf)
                .ToList();
        }

        //public List<Estado> GetByDatNascimento(DateTime dataInicial, DateTime dataFinal)
        //{
        //    return db.Estados
        //        .Where(x => x.DataNascimento >= dataInicial && x.DataNascimento <= dataFinal)
        //        .OrderBy(x => x.Nome)
        //        .ToList();
        //}

    }
}
