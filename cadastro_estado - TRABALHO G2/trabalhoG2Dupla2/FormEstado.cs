﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using trabalhoG2Dupla.Entidades;
using trabalhoG2Dupla.Dao;

namespace aula0903ex1
{
    public partial class FormEstado : Form
    {
        private Dao Dao;

        public FormEstado()
        {
            InitializeComponent();
            Dao = new Dao();
            PopularCombobox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void FrmPrincipal_FormClosing(object sender, FormClosedEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
        private void PopularCombobox()
        {
            //cmbExemplo.DataSource = pessoaDao.GetAll();
            //cmbExemplo.DisplayMember = "Nome";
            //cmbExemplo.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show(cmbExemplo.SelectedValue.ToString());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmPessoaPesquisa();
            formulario.ShowDialog();
        }

        private void salvar_Click(object sender, EventArgs e)
        {
            var pessoa = new Estado();
            pessoa.Nome = txtNome.Text;
            pessoa.Peso = float.Parse(txtPeso.Text);
            pessoa.DataNascimento = DateTime.Parse(txtdtanasc.Text);
            pessoaDao.Adicionar(pessoa);
        }

        private void resultdtanasc_TextChanged(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {
            int cod = int.Parse(txtcodigo.Text);

           var obj = pessoaDao.GetById(cod);

            if(obj != null)
            {
                txtNome.Text = obj.Nome;
                txtPeso.Text = obj.Peso.ToString();
                txtdtanasc.Text = obj.DataNascimento.ToString("dd/MM/yyyy");
            }
        
           
          
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
