﻿namespace aula0903ex1
{
    partial class FormEstado
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.codigo = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.nomebusca = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.novo = new System.Windows.Forms.Button();
            this.pesquisar = new System.Windows.Forms.Button();
            this.nomepesquisa = new System.Windows.Forms.Label();
            this.pesopesquisa = new System.Windows.Forms.Label();
            this.datanasc = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtdtanasc = new System.Windows.Forms.TextBox();
            this.excluir = new System.Windows.Forms.Button();
            this.cancelar = new System.Windows.Forms.Button();
            this.salvar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(50, 43);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(40, 13);
            this.codigo.TabIndex = 3;
            this.codigo.Text = "Código";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Location = new System.Drawing.Point(109, 40);
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(68, 20);
            this.txtcodigo.TabIndex = 4;
            // 
            // nomebusca
            // 
            this.nomebusca.Location = new System.Drawing.Point(200, 40);
            this.nomebusca.Name = "nomebusca";
            this.nomebusca.Size = new System.Drawing.Size(163, 20);
            this.nomebusca.TabIndex = 5;
            this.nomebusca.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(396, 38);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 6;
            this.ok.Text = "Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // novo
            // 
            this.novo.Location = new System.Drawing.Point(477, 38);
            this.novo.Name = "novo";
            this.novo.Size = new System.Drawing.Size(75, 23);
            this.novo.TabIndex = 7;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            // 
            // pesquisar
            // 
            this.pesquisar.Location = new System.Drawing.Point(558, 38);
            this.pesquisar.Name = "pesquisar";
            this.pesquisar.Size = new System.Drawing.Size(75, 23);
            this.pesquisar.TabIndex = 8;
            this.pesquisar.Text = "Pesquisar";
            this.pesquisar.UseVisualStyleBackColor = true;
            this.pesquisar.Click += new System.EventHandler(this.pesquisar_Click);
            // 
            // nomepesquisa
            // 
            this.nomepesquisa.AutoSize = true;
            this.nomepesquisa.Location = new System.Drawing.Point(226, 69);
            this.nomepesquisa.Name = "nomepesquisa";
            this.nomepesquisa.Size = new System.Drawing.Size(21, 13);
            this.nomepesquisa.TabIndex = 9;
            this.nomepesquisa.Text = "UF";
            // 
            // pesopesquisa
            // 
            this.pesopesquisa.AutoSize = true;
            this.pesopesquisa.Location = new System.Drawing.Point(178, 109);
            this.pesopesquisa.Name = "pesopesquisa";
            this.pesopesquisa.Size = new System.Drawing.Size(62, 13);
            this.pesopesquisa.TabIndex = 10;
            this.pesopesquisa.Text = "UF Extenso";
            // 
            // datanasc
            // 
            this.datanasc.AutoSize = true;
            this.datanasc.Location = new System.Drawing.Point(207, 32);
            this.datanasc.Name = "datanasc";
            this.datanasc.Size = new System.Drawing.Size(40, 13);
            this.datanasc.TabIndex = 11;
            this.datanasc.Text = "Cidade";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(263, 66);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(258, 20);
            this.txtNome.TabIndex = 12;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(263, 106);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(258, 20);
            this.txtPeso.TabIndex = 13;
            // 
            // txtdtanasc
            // 
            this.txtdtanasc.Location = new System.Drawing.Point(263, 29);
            this.txtdtanasc.Name = "txtdtanasc";
            this.txtdtanasc.Size = new System.Drawing.Size(258, 20);
            this.txtdtanasc.TabIndex = 14;
            this.txtdtanasc.TextChanged += new System.EventHandler(this.resultdtanasc_TextChanged);
            // 
            // excluir
            // 
            this.excluir.Location = new System.Drawing.Point(131, 158);
            this.excluir.Name = "excluir";
            this.excluir.Size = new System.Drawing.Size(75, 23);
            this.excluir.TabIndex = 15;
            this.excluir.Text = "Excluir";
            this.excluir.UseVisualStyleBackColor = true;
            // 
            // cancelar
            // 
            this.cancelar.Location = new System.Drawing.Point(301, 158);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(75, 23);
            this.cancelar.TabIndex = 16;
            this.cancelar.Text = "Cancelar";
            this.cancelar.UseVisualStyleBackColor = true;
            // 
            // salvar
            // 
            this.salvar.Location = new System.Drawing.Point(424, 158);
            this.salvar.Name = "salvar";
            this.salvar.Size = new System.Drawing.Size(75, 23);
            this.salvar.TabIndex = 17;
            this.salvar.Text = "Salvar";
            this.salvar.UseVisualStyleBackColor = true;
            this.salvar.Click += new System.EventHandler(this.salvar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pesquisar);
            this.groupBox1.Controls.Add(this.codigo);
            this.groupBox1.Controls.Add(this.txtcodigo);
            this.groupBox1.Controls.Add(this.nomebusca);
            this.groupBox1.Controls.Add(this.ok);
            this.groupBox1.Controls.Add(this.novo);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(676, 85);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisa";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.salvar);
            this.groupBox2.Controls.Add(this.cancelar);
            this.groupBox2.Controls.Add(this.excluir);
            this.groupBox2.Controls.Add(this.txtdtanasc);
            this.groupBox2.Controls.Add(this.txtPeso);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.datanasc);
            this.groupBox2.Controls.Add(this.pesopesquisa);
            this.groupBox2.Controls.Add(this.nomepesquisa);
            this.groupBox2.Location = new System.Drawing.Point(12, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(672, 192);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resultado";
            // 
            // FormEstado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 315);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormEstado";
            this.Text = "Cadastro de Pessoa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.TextBox nomebusca;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.Button pesquisar;
        private System.Windows.Forms.Label nomepesquisa;
        private System.Windows.Forms.Label pesopesquisa;
        private System.Windows.Forms.Label datanasc;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.TextBox txtdtanasc;
        private System.Windows.Forms.Button excluir;
        private System.Windows.Forms.Button cancelar;
        private System.Windows.Forms.Button salvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

