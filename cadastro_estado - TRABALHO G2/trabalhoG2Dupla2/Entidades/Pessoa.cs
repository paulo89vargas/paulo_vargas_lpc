﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trabalhoG2Dupla.Entidades;

namespace trabalhoG2Dupla.Entidades
{

    public class Estado
    {
        public virtual int Id { get; set; }

        public virtual string Uf { get; set; }

        public virtual string UfExtenso { get; set; }

        public virtual List<Cidade> Cidades { get; set; }


    }
}
