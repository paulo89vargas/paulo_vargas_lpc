﻿using aula0903ex1.Entidades;

using System;
using System.Collections.Generic;
using System.Linq;

namespace aula0903ex1.Dao
{
    public class PessoaDao
    {
        List<Pessoa> datasource = new List<Pessoa>();
        private AppContexto db;

        public PessoaDao()
        {
            db = new AppContexto();

            
        }

        public List<Pessoa> GetAll()
        {
     
            return db.Pessoas.ToList();

        }
        public Pessoa GetById(int id)
        {

            return db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
        }
        public List<Pessoa> GetByCPF(int cpf)
        {
      
            return db.Pessoas.Where(x => x.Cpf == cpf).ToList();
        }
        public List<Pessoa> GetByRg(int rg)
        {
         
            return db.Pessoas.Where(x => x.Rg == rg).ToList();
        }

        public Pessoa GetByNome(string nome)
        {
      
            return db.Pessoas.Where(x => x.Nome == nome).FirstOrDefault();
        }

        public void Adicionar(Pessoa pessoa)
        {

            db.Pessoas.Add(pessoa);
            db.SaveChanges();
        }

        public void Atualizar(Pessoa pessoa)
        {
            var obj = db.Pessoas.Where(x => x.Id == pessoa.Id).FirstOrDefault();
            if (obj != null)
            {
                    obj.Cpf = pessoa.Cpf;
                    obj.Rg = pessoa.Rg;
                    obj.Nome = pessoa.Nome;                    
                    db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Pessoas.Remove(obj);
                db.SaveChanges();
            }
        }
       

        //public List<Pessoa> GetByRg(int rg)
        //{
        //    return db.Pessoas
        //        .Where(x => x.Rg == rg)
        //        .OrderBy(x => x.Nome)
        //        .ToList();
        //}

        //public List<Pessoa> GetByDatNascimento(DateTime dataInicial, DateTime dataFinal)
        //{
        //    return db.Pessoas
        //        .Where(x => x.DataNascimento >= dataInicial && x.DataNascimento <= dataFinal)
        //        .OrderBy(x => x.Nome)
        //        .ToList();
        //}

    }
}
