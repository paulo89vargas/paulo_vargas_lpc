﻿using aula0903ex1.Entidades;

using System;
using System.Collections.Generic;
using System.Linq;

namespace aula0903ex1.Dao
{
    public class PessoaDao
    {
        List<Pessoa> datasource = new List<Pessoa>();
        private AppContexto db;

        public PessoaDao()
        {
            db = new AppContexto();

            datasource = new List<Pessoa>();

            }

        public List<Pessoa> GetAll()
        {
     
            return db.Pessoas.ToList();

        }

        public List<Pessoa> GetByNome(string nome)
        {
      
            return db.Pessoas.Where(x => x.Nome == nome).ToList();
        }
        public List<Pessoa> GetByPeso(Int32 peso)
        {
         
            return db.Pessoas.Where(x => x.Peso == peso).ToList();
        }

        public Pessoa GetById(int id)
        {
      
            return db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Adicionar(Pessoa pessoa)
        {

            db.Pessoas.Add(pessoa);
            db.SaveChanges();
        }

        public void Atualizar(Pessoa pessoa)
        {
            var obj = db.Pessoas.Where(x => x.Id == pessoa.Id).FirstOrDefault();
            if (obj != null)
            {
                obj.Peso = pessoa.Peso;
                obj.Nome = pessoa.Nome;
                obj.DataNascimento = pessoa.DataNascimento;

                db.SaveChanges();
            }
        }

        public void Excluir(int id)
        {
            var obj = db.Pessoas.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Pessoas.Remove(obj);
                db.SaveChanges();
            }
        }

        public List<Pessoa> GetByDatNascimento(DateTime data)
        {
            return db.Pessoas
                .Where(x => x.DataNascimento == data)
                .OrderBy(x => x.Nome)
                .ToList();
        }

        public List<Pessoa> GetByDatNascimento(DateTime dataInicial, DateTime dataFinal)
        {
            return db.Pessoas
                .Where(x => x.DataNascimento >= dataInicial && x.DataNascimento <= dataFinal)
                .OrderBy(x => x.Nome)
                .ToList();
        }

    }
}
