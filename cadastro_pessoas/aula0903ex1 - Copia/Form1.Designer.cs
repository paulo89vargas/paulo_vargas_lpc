﻿namespace aula0903ex1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.codigo = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.nomebusca = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.novo = new System.Windows.Forms.Button();
            this.pesquisar = new System.Windows.Forms.Button();
            this.nomepesquisa = new System.Windows.Forms.Label();
            this.pesopesquisa = new System.Windows.Forms.Label();
            this.datanasc = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtValorInscri = new System.Windows.Forms.TextBox();
            this.txtIdade = new System.Windows.Forms.TextBox();
            this.excluir = new System.Windows.Forms.Button();
            this.txtEditar = new System.Windows.Forms.Button();
            this.salvar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSemestre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRG = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(50, 43);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(40, 13);
            this.codigo.TabIndex = 3;
            this.codigo.Text = "Código";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Location = new System.Drawing.Point(109, 40);
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(68, 20);
            this.txtcodigo.TabIndex = 4;
            // 
            // nomebusca
            // 
            this.nomebusca.Location = new System.Drawing.Point(200, 40);
            this.nomebusca.Name = "nomebusca";
            this.nomebusca.Size = new System.Drawing.Size(163, 20);
            this.nomebusca.TabIndex = 5;
            this.nomebusca.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(396, 38);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 6;
            this.ok.Text = "Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // novo
            // 
            this.novo.Location = new System.Drawing.Point(477, 38);
            this.novo.Name = "novo";
            this.novo.Size = new System.Drawing.Size(75, 23);
            this.novo.TabIndex = 7;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            // 
            // pesquisar
            // 
            this.pesquisar.Location = new System.Drawing.Point(558, 38);
            this.pesquisar.Name = "pesquisar";
            this.pesquisar.Size = new System.Drawing.Size(75, 23);
            this.pesquisar.TabIndex = 8;
            this.pesquisar.Text = "Pesquisar";
            this.pesquisar.UseVisualStyleBackColor = true;
            this.pesquisar.Click += new System.EventHandler(this.pesquisar_Click);
            // 
            // nomepesquisa
            // 
            this.nomepesquisa.AutoSize = true;
            this.nomepesquisa.Location = new System.Drawing.Point(125, 110);
            this.nomepesquisa.Name = "nomepesquisa";
            this.nomepesquisa.Size = new System.Drawing.Size(82, 13);
            this.nomepesquisa.TabIndex = 9;
            this.nomepesquisa.Text = "Nome Completo";
            this.nomepesquisa.Click += new System.EventHandler(this.nomepesquisa_Click);
            // 
            // pesopesquisa
            // 
            this.pesopesquisa.AutoSize = true;
            this.pesopesquisa.Location = new System.Drawing.Point(115, 223);
            this.pesopesquisa.Name = "pesopesquisa";
            this.pesopesquisa.Size = new System.Drawing.Size(92, 13);
            this.pesopesquisa.TabIndex = 10;
            this.pesopesquisa.Text = "Valor da Inscrição";
            // 
            // datanasc
            // 
            this.datanasc.AutoSize = true;
            this.datanasc.Location = new System.Drawing.Point(125, 147);
            this.datanasc.Name = "datanasc";
            this.datanasc.Size = new System.Drawing.Size(34, 13);
            this.datanasc.TabIndex = 11;
            this.datanasc.Text = "Idade";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(213, 107);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(258, 20);
            this.txtNome.TabIndex = 12;
            // 
            // txtValorInscri
            // 
            this.txtValorInscri.Location = new System.Drawing.Point(213, 220);
            this.txtValorInscri.Name = "txtValorInscri";
            this.txtValorInscri.Size = new System.Drawing.Size(258, 20);
            this.txtValorInscri.TabIndex = 13;
            // 
            // txtIdade
            // 
            this.txtIdade.Location = new System.Drawing.Point(213, 144);
            this.txtIdade.Name = "txtIdade";
            this.txtIdade.Size = new System.Drawing.Size(258, 20);
            this.txtIdade.TabIndex = 14;
            this.txtIdade.TextChanged += new System.EventHandler(this.resultdtanasc_TextChanged);
            // 
            // excluir
            // 
            this.excluir.Location = new System.Drawing.Point(129, 282);
            this.excluir.Name = "excluir";
            this.excluir.Size = new System.Drawing.Size(75, 23);
            this.excluir.TabIndex = 15;
            this.excluir.Text = "Excluir";
            this.excluir.UseVisualStyleBackColor = true;
            this.excluir.Click += new System.EventHandler(this.excluir_Click);
            // 
            // txtEditar
            // 
            this.txtEditar.Location = new System.Drawing.Point(299, 282);
            this.txtEditar.Name = "txtEditar";
            this.txtEditar.Size = new System.Drawing.Size(75, 23);
            this.txtEditar.TabIndex = 16;
            this.txtEditar.Text = "Editar";
            this.txtEditar.UseVisualStyleBackColor = true;
            this.txtEditar.Click += new System.EventHandler(this.editar_Click);
            // 
            // salvar
            // 
            this.salvar.Location = new System.Drawing.Point(422, 282);
            this.salvar.Name = "salvar";
            this.salvar.Size = new System.Drawing.Size(75, 23);
            this.salvar.TabIndex = 17;
            this.salvar.Text = "Salvar";
            this.salvar.UseVisualStyleBackColor = true;
            this.salvar.Click += new System.EventHandler(this.salvar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pesquisar);
            this.groupBox1.Controls.Add(this.codigo);
            this.groupBox1.Controls.Add(this.txtcodigo);
            this.groupBox1.Controls.Add(this.nomebusca);
            this.groupBox1.Controls.Add(this.ok);
            this.groupBox1.Controls.Add(this.novo);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(676, 85);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisar";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSemestre);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtRG);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtCPF);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.salvar);
            this.groupBox2.Controls.Add(this.txtEditar);
            this.groupBox2.Controls.Add(this.excluir);
            this.groupBox2.Controls.Add(this.txtIdade);
            this.groupBox2.Controls.Add(this.txtValorInscri);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.datanasc);
            this.groupBox2.Controls.Add(this.pesopesquisa);
            this.groupBox2.Controls.Add(this.nomepesquisa);
            this.groupBox2.Location = new System.Drawing.Point(12, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(672, 349);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cadastrar";
            // 
            // txtSemestre
            // 
            this.txtSemestre.Location = new System.Drawing.Point(213, 181);
            this.txtSemestre.Name = "txtSemestre";
            this.txtSemestre.Size = new System.Drawing.Size(258, 20);
            this.txtSemestre.TabIndex = 23;
            this.txtSemestre.TextChanged += new System.EventHandler(this.textSemestre_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Semestre";
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(213, 58);
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(258, 20);
            this.txtRG.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "RG";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(213, 19);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(258, 20);
            this.txtCPF.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(150, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "CPF";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 508);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Cadastro de Pessoa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.TextBox nomebusca;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.Button pesquisar;
        private System.Windows.Forms.Label nomepesquisa;
        private System.Windows.Forms.Label pesopesquisa;
        private System.Windows.Forms.Label datanasc;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtValorInscri;
        private System.Windows.Forms.TextBox txtIdade;
        private System.Windows.Forms.Button excluir;
        private System.Windows.Forms.Button txtEditar;
        private System.Windows.Forms.Button salvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRG;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSemestre;
        private System.Windows.Forms.Label label3;
    }
}

