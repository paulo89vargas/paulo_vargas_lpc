﻿using aula0903ex1.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula0903ex1
{
    public class AppContexto : DbContext
    {
        public DbSet<Pessoa> Pessoas { get; set; }
    }
}
