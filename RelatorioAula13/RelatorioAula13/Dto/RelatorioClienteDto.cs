﻿namespace RelatorioAula13.Dto
{
    public class RelatorioClienteDto
    {
        public int IdCliente { get; set; }

        public string NomeCliente { get; set; }
    }
}
