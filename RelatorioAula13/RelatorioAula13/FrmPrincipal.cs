﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RelatorioAula13;
using RelatorioAula13.Dao;
using RelatorioAula13.Dto;
using RelatorioAula13.Entidades;
using RelatorioAula13.Relatorio;
using RelatorioAula13.Properties;
using Microsoft.Reporting.WinForms;


namespace RelatorioAula13.Relatorio
{
    
    public partial class FrmPrincipal : Form
    {
        private ReportViewer reportViewer1;

        public FrmPrincipal()
        {
            InitializeComponent();
			
			CarregarDataSet();
        }
		private void FrmRelatorioCliente_Load( object sender, EventArgs e)
		{
			this.reportViewer1.RefreshReport();			
		}
		private void CarregarDataSet()
		{
			var dao = new ClienteDao();
			
			var lista = dao.GetAll().Select( x => new RelatorioClienteDto {
				
				IdCliente = x.Id,
				NomeCliente = x.Nome
				
			}).ToList();
			
			var rs = new ReportDataSource();
			rs.Name = "DataSetCliente";
			rs.Value = lista;

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(rs);
        }

        private void FrmRelatorioCliente_Load_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDataSet();
        }
    }
}
