﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RelatorioAula13.Dao;
using RelatorioAula13.Dto;
using RelatorioAula13.Entidades;
using RelatorioAula13.Relatorio;
using RelatorioAula13.Properties;
using RelatorioAula13;
using Microsoft.Reporting.WinForms;

namespace RelatorioAula13
{
    public partial class FrmRelatorioCliente : Form
    {
        private BindingSource RelatorioClienteDtoBindingSource;
        private IContainer components;
        private ReportViewer reportViewer1;
        private Label label1;
        private TextBox textBox1;
        private Button btnPesquisar_Click;
        private ReportViewer reportViewer3;
        private ReportViewer reportViewer2;

        public FrmRelatorioCliente()
        {
            InitializeComponent();
			
			CarregarDataSet();
        }
		private void FrmRelatorioCliente_Load( object sender, EventArgs e)
		{
			this.reportViewer1.RefreshReport();			
		}
		private void CarregarDataSet()
		{
			var dao = new ClienteDao();
			
			var lista = dao.GetAll().Select( x => new RelatorioClienteDto {
				
				IdCliente = x.Id,
				NomeCliente = x.Nome
				
			}).ToList();
			
			var rs = new ReportDataSource();
			rs.Name = "DataSetCliente";
			rs.Value = lista;
			
			//reportViewer1.LocalReport.DataSources.Clear();
			//reportViewer1.LocalReport.DataSources.Add(rs);
		}

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.RelatorioClienteDtoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.reportViewer3 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioClienteDtoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // RelatorioClienteDtoBindingSource
            // 
            this.RelatorioClienteDtoBindingSource.DataMember = "RelatorioClienteDto";
            // 
            // reportViewer2
            // 
            reportDataSource1.Name = "DataSetCliente";
            reportDataSource1.Value = this.RelatorioClienteDtoBindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "RelatorioAula13.Relatorio.RelatorioCliente.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(12, 85);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ServerReport.BearerToken = null;
            this.reportViewer2.Size = new System.Drawing.Size(673, 321);
            this.reportViewer2.TabIndex = 1;
            this.reportViewer2.Load += new System.EventHandler(this.reportViewer2_Load);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(77, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(204, 20);
            this.textBox1.TabIndex = 3;
            // 
            // reportViewer3
            // 
            reportDataSource2.Name = "DataSetCliente";
            reportDataSource2.Value = this.RelatorioClienteDtoBindingSource;
            this.reportViewer3.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer3.LocalReport.ReportEmbeddedResource = "RelatorioAula13.Relatorio.RelatorioCliente.rdlc";
            this.reportViewer3.Location = new System.Drawing.Point(12, 53);
            this.reportViewer3.Name = "reportViewer3";
            this.reportViewer3.ServerReport.BearerToken = null;
            this.reportViewer3.Size = new System.Drawing.Size(609, 281);
            this.reportViewer3.TabIndex = 0;
            // 
            // FrmRelatorioCliente
            // 
            this.ClientSize = new System.Drawing.Size(633, 346);
            this.Controls.Add(this.reportViewer3);
            this.Name = "FrmRelatorioCliente";
            this.Load += new System.EventHandler(this.FrmRelatorioCliente_Load_2);
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioClienteDtoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        private void FrmRelatorioCliente_Load_1(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
            this.reportViewer2.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void reportViewer2_Load(object sender, EventArgs e)
        {

        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            var dao = new ClienteDao();

            var lista = dao.GetAll().Select(x => new RelatorioClienteDto
            {

                IdCliente = x.Id,
                NomeCliente = x.Nome

            }).ToList();

            var rs = new ReportDataSource();
            rs.Name = "DataSetCliente";
            rs.Value = lista;

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(rs);
        }

        private void FrmRelatorioCliente_Load_2(object sender, EventArgs e)
        {

            this.reportViewer3.RefreshReport();
        }
    }
    }
