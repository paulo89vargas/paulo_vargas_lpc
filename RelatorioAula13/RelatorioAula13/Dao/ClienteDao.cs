﻿using RelatorioAula13.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace RelatorioAula13.Dao
{
    public class ClienteDao
    {
        private List<Cliente> datasource;

        public ClienteDao()
        {
            datasource = new List<Cliente>();

            datasource.Add(new Cliente { Id = 1, Nome = "Pedro" });
            datasource.Add(new Cliente { Id = 2, Nome = "Juca" });
            datasource.Add(new Cliente { Id = 3, Nome = "Maria" });
        }

        public List<Cliente> GetAll()
        {
            return datasource;
        }

        public List<Cliente> GetByNome(string nome)
        {
            return datasource.Where(x => x.Nome.Contains(nome)).ToList();
        }
    }
}
