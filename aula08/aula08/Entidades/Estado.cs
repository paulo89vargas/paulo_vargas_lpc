﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula08.Entidades
{
    public class Estado
    {
        public int Id { get; set; }
        public string UF { get; set; }
        public string UFExtenso { get; set; }
    }
}
