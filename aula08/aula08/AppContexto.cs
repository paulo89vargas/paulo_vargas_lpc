﻿using aula08.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace aula08
{
    public class AppContexto : DbContext
    {
        public AppContexto() : base() { }

        public DbSet<Estado> Estados { get; set; }
    }
}
