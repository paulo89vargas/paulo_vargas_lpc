﻿using registrodetemperaturas.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace registrodetemperaturas
{
    public class AppContexto : DbContext
    {
        public DbSet<Registro> Registro { get; set; }
    }
}
