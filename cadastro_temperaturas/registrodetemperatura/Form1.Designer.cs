﻿namespace aula0903ex1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.codigo = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.nomebusca = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.pesquisar = new System.Windows.Forms.Button();
            this.nomepesquisa = new System.Windows.Forms.Label();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.excluir = new System.Windows.Forms.Button();
            this.txtEditar = new System.Windows.Forms.Button();
            this.salvar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTemperatura = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(50, 43);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(30, 13);
            this.codigo.TabIndex = 3;
            this.codigo.Text = "Data";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Location = new System.Drawing.Point(109, 40);
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(68, 20);
            this.txtcodigo.TabIndex = 4;
            // 
            // nomebusca
            // 
            this.nomebusca.Location = new System.Drawing.Point(200, 40);
            this.nomebusca.Name = "nomebusca";
            this.nomebusca.Size = new System.Drawing.Size(163, 20);
            this.nomebusca.TabIndex = 5;
            this.nomebusca.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(396, 38);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 6;
            this.ok.Text = "Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // pesquisar
            // 
            this.pesquisar.Location = new System.Drawing.Point(498, 38);
            this.pesquisar.Name = "pesquisar";
            this.pesquisar.Size = new System.Drawing.Size(75, 23);
            this.pesquisar.TabIndex = 8;
            this.pesquisar.Text = "Pesquisar";
            this.pesquisar.UseVisualStyleBackColor = true;
            this.pesquisar.Click += new System.EventHandler(this.pesquisar_Click);
            // 
            // nomepesquisa
            // 
            this.nomepesquisa.AutoSize = true;
            this.nomepesquisa.Location = new System.Drawing.Point(125, 110);
            this.nomepesquisa.Name = "nomepesquisa";
            this.nomepesquisa.Size = new System.Drawing.Size(70, 13);
            this.nomepesquisa.TabIndex = 9;
            this.nomepesquisa.Text = "Observações";
            this.nomepesquisa.Click += new System.EventHandler(this.nomepesquisa_Click);
            // 
            // txtObservacao
            // 
            this.txtObservacao.Location = new System.Drawing.Point(213, 107);
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(258, 20);
            this.txtObservacao.TabIndex = 12;
            // 
            // excluir
            // 
            this.excluir.Location = new System.Drawing.Point(153, 165);
            this.excluir.Name = "excluir";
            this.excluir.Size = new System.Drawing.Size(75, 23);
            this.excluir.TabIndex = 15;
            this.excluir.Text = "Excluir";
            this.excluir.UseVisualStyleBackColor = true;
            this.excluir.Click += new System.EventHandler(this.excluir_Click);
            // 
            // txtEditar
            // 
            this.txtEditar.Location = new System.Drawing.Point(299, 165);
            this.txtEditar.Name = "txtEditar";
            this.txtEditar.Size = new System.Drawing.Size(75, 23);
            this.txtEditar.TabIndex = 16;
            this.txtEditar.Text = "Editar";
            this.txtEditar.UseVisualStyleBackColor = true;
            this.txtEditar.Click += new System.EventHandler(this.editar_Click);
            // 
            // salvar
            // 
            this.salvar.Location = new System.Drawing.Point(422, 165);
            this.salvar.Name = "salvar";
            this.salvar.Size = new System.Drawing.Size(75, 23);
            this.salvar.TabIndex = 17;
            this.salvar.Text = "Salvar";
            this.salvar.UseVisualStyleBackColor = true;
            this.salvar.Click += new System.EventHandler(this.salvar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pesquisar);
            this.groupBox1.Controls.Add(this.codigo);
            this.groupBox1.Controls.Add(this.txtcodigo);
            this.groupBox1.Controls.Add(this.nomebusca);
            this.groupBox1.Controls.Add(this.ok);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(676, 85);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisar Registro";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTemperatura);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtData);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.salvar);
            this.groupBox2.Controls.Add(this.txtEditar);
            this.groupBox2.Controls.Add(this.excluir);
            this.groupBox2.Controls.Add(this.txtObservacao);
            this.groupBox2.Controls.Add(this.nomepesquisa);
            this.groupBox2.Location = new System.Drawing.Point(12, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(672, 215);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cadastrar Registro";
            // 
            // txtTemperatura
            // 
            this.txtTemperatura.Location = new System.Drawing.Point(213, 58);
            this.txtTemperatura.Name = "txtTemperatura";
            this.txtTemperatura.Size = new System.Drawing.Size(258, 20);
            this.txtTemperatura.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Temperatura em Graus Celcius";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(213, 19);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(258, 20);
            this.txtData.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(150, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Data";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 338);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Cadastro de Leituras";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.TextBox nomebusca;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button pesquisar;
        private System.Windows.Forms.Label nomepesquisa;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.Button excluir;
        private System.Windows.Forms.Button txtEditar;
        private System.Windows.Forms.Button salvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTemperatura;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label1;
    }
}

