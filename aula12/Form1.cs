﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using aula0903ex1.Entidades;
using aula0903ex1.Dao;
using aula1603;

namespace aula0903ex1
{
    public partial class Form1 : Form
    {
        private PessoaDao pessoaDao;

        public Form1()
        {
            InitializeComponent();
            pessoaDao = new PessoaDao();
            PopularCombobox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void FrmPrincipal_FormClosing(object sender, FormClosedEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
        private void PopularCombobox()
        {
            //cmbExemplo.DataSource = pessoaDao.GetAll();
            //cmbExemplo.DisplayMember = "Nome";
            //cmbExemplo.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show(cmbExemplo.SelectedValue.ToString());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmPessoaPesquisa(pessoaDao);
            formulario.ShowDialog();
        }

        private void salvar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();
            pessoa.Nome = txtNome.Text;
            pessoa.Peso = float.Parse(txtPeso.Text);
            pessoa.DataNascimento = DateTime.Parse(txtdtanasc.Text);
            pessoaDao.Adicionar(pessoa);
        }

        private void resultdtanasc_TextChanged(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {
            int cod = int.Parse(txtcodigo.Text);

           var obj = pessoaDao.GetById(cod);

            if(obj != null)
            {
                txtNome.Text = obj.Nome;
                txtPeso.Text = obj.Peso.ToString();
                txtdtanasc.Text = obj.DataNascimento.ToString("dd/MM/yyyy");
            }
        
           
          
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void novo_Click(object sender, EventArgs e)
        {
            groupBox2.Enabled = true;
            groupBox1.Enabled = false;
        }
    }
}
