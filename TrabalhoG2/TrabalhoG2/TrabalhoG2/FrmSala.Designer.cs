﻿namespace TrabalhoG2
{
    partial class FrmSala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cADASTRARSALAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNTERDITARSALAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEMOVERSALAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NumeroSala = new System.Windows.Forms.TextBox();
            this.CapacidadeSala = new System.Windows.Forms.TextBox();
            this.btnConfirmaSala = new System.Windows.Forms.Button();
            this.btnLimpaSala = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cADASTRARSALAToolStripMenuItem,
            this.iNTERDITARSALAToolStripMenuItem,
            this.rEMOVERSALAToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cADASTRARSALAToolStripMenuItem
            // 
            this.cADASTRARSALAToolStripMenuItem.Name = "cADASTRARSALAToolStripMenuItem";
            this.cADASTRARSALAToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.cADASTRARSALAToolStripMenuItem.Text = "CADASTRAR SALA";
            // 
            // iNTERDITARSALAToolStripMenuItem
            // 
            this.iNTERDITARSALAToolStripMenuItem.Name = "iNTERDITARSALAToolStripMenuItem";
            this.iNTERDITARSALAToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.iNTERDITARSALAToolStripMenuItem.Text = "INTERDITAR SALA";
            // 
            // rEMOVERSALAToolStripMenuItem
            // 
            this.rEMOVERSALAToolStripMenuItem.Name = "rEMOVERSALAToolStripMenuItem";
            this.rEMOVERSALAToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.rEMOVERSALAToolStripMenuItem.Text = "REMOVER SALA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NÚMERO DA SALA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "CAPACIDADE";
            // 
            // NumeroSala
            // 
            this.NumeroSala.Location = new System.Drawing.Point(170, 104);
            this.NumeroSala.Name = "NumeroSala";
            this.NumeroSala.Size = new System.Drawing.Size(100, 20);
            this.NumeroSala.TabIndex = 3;
            // 
            // CapacidadeSala
            // 
            this.CapacidadeSala.Location = new System.Drawing.Point(170, 179);
            this.CapacidadeSala.Name = "CapacidadeSala";
            this.CapacidadeSala.Size = new System.Drawing.Size(100, 20);
            this.CapacidadeSala.TabIndex = 4;
            // 
            // btnConfirmaSala
            // 
            this.btnConfirmaSala.Location = new System.Drawing.Point(170, 282);
            this.btnConfirmaSala.Name = "btnConfirmaSala";
            this.btnConfirmaSala.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmaSala.TabIndex = 5;
            this.btnConfirmaSala.Text = "CONFIRMAR";
            this.btnConfirmaSala.UseVisualStyleBackColor = true;
            // 
            // btnLimpaSala
            // 
            this.btnLimpaSala.Location = new System.Drawing.Point(505, 282);
            this.btnLimpaSala.Name = "btnLimpaSala";
            this.btnLimpaSala.Size = new System.Drawing.Size(75, 23);
            this.btnLimpaSala.TabIndex = 6;
            this.btnLimpaSala.Text = "LIMPAR";
            this.btnLimpaSala.UseVisualStyleBackColor = true;
            // 
            // FrmSala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnLimpaSala);
            this.Controls.Add(this.btnConfirmaSala);
            this.Controls.Add(this.CapacidadeSala);
            this.Controls.Add(this.NumeroSala);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmSala";
            this.Text = "Cadastrar Sala";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cADASTRARSALAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNTERDITARSALAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEMOVERSALAToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NumeroSala;
        private System.Windows.Forms.TextBox CapacidadeSala;
        private System.Windows.Forms.Button btnConfirmaSala;
        private System.Windows.Forms.Button btnLimpaSala;
    }
}