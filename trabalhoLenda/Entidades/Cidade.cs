﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula0903ex1.Entidades
{

    public class Pessoa
    {
        public int Id { get; set; }

        public int Cpf { get; set; }

        public int Rg { get; set; }

        public string VlrInsc { get; set; }

        public string Nome { get; set; }

        public int Idade { get; set; }

        public int Semestre { get; set; }

    }
}
