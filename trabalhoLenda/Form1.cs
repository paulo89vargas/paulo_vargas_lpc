﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using aula0903ex1.Entidades;
using aula0903ex1.Dao;
using aula1603;

namespace aula0903ex1
{
    public partial class Form1 : Form
    {
        private string InscriA = "30";
        private string InscriB = "20";
        private string InscriC = "10";
        private Dao.Dao pessoaDao;

        public Form1()
        {
            InitializeComponent();
            pessoaDao = new Dao.Dao();
            PopularCombobox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void FrmPrincipal_FormClosing(object sender, FormClosedEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
        private void PopularCombobox()
        {
            //cmbExemplo.DataSource = pessoaDao.GetAll();
            //cmbExemplo.DisplayMember = "Nome";
            //cmbExemplo.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show(cmbExemplo.SelectedValue.ToString());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pesquisar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmPessoaPesquisa(pessoaDao);
            formulario.ShowDialog();
        }

        private void salvar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();



            pessoa.Cpf = int.Parse(txtCPF.Text);
            pessoa.Rg = Int32.Parse(txtRG.Text);
            pessoa.Nome = txtNome.Text;
            pessoa.Idade = int.Parse(txtIdade.Text);
            pessoa.Semestre = int.Parse(txtSemestre.Text);
            if (pessoa.Semestre < 3)
            { pessoa.VlrInsc = "30"; }
            if (pessoa.Semestre < 7)
            { pessoa.VlrInsc = "20"; }
            if (pessoa.Semestre >= 7)
            { pessoa.VlrInsc = "10"; }

            txtValorInscri.Text = pessoa.VlrInsc;
            pessoaDao.Adicionar(pessoa);
        }

        private void resultdtanasc_TextChanged(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {
            int cod = int.Parse(txtcodigo.Text);

            var obj = pessoaDao.GetById(cod);

            if (obj != null)
            {
                            
                txtCPF.Text = obj.Cpf.ToString();
                txtRG.Text = obj.Rg.ToString();
                txtNome.Text = obj.Nome.ToString();
                txtValorInscri.Text = obj.VlrInsc.ToString();
                txtIdade.Text = obj.Idade.ToString();
                txtSemestre.Text = obj.Semestre.ToString();
            }



        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void nomepesquisa_Click(object sender, EventArgs e)
        {

        }

        private void editar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();



            pessoa.Cpf = int.Parse(txtCPF.Text);
            pessoa.Rg = Int32.Parse(txtRG.Text);
            pessoa.Nome = txtNome.Text;
            pessoa.Idade = int.Parse(txtIdade.Text);
            pessoa.Semestre = int.Parse(txtSemestre.Text);
            if (pessoa.Semestre < 3)
            { pessoa.VlrInsc = "30"; }
            if (pessoa.Semestre < 7)
            { pessoa.VlrInsc = "20"; }
            if (pessoa.Semestre >= 7)
            { pessoa.VlrInsc = "10"; }

            txtValorInscri.Text = pessoa.VlrInsc;
            pessoaDao.Atualizar(pessoa);
        }

        private void excluir_Click(object sender, EventArgs e)
        {
            pessoaDao.Excluir(int.Parse(txtcodigo.Text));

        }

        private void textSemestre_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
