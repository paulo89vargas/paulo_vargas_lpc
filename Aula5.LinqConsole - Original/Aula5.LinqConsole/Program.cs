﻿using Aula5.LinqConsole.Dao;
using Aula5.LinqConsole.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aula5.LinqConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var dao = new PessoaDao();

            var pessoa = dao.GetByNome("Pedro");
            
            var listDtaUniq = dao.GetByDatNascimento(new DateTime(2000, 8, 20));
            
            var listDta = dao.GetByDatNascimento(new DateTime(1980, 01, 01), new DateTime(1990, 12, 31));

            // Criacao
            var newPessoa = new Pessoa();
            newPessoa.Id = 1000;
            newPessoa.Nome = "Juca";
            newPessoa.Peso = 90;
            newPessoa.DataNascimento = new DateTime(2000, 01, 01);
            dao.Adicionar(newPessoa);

            var obj = dao.GetByPeso(45);
            // Edicao
            
            // Exclusao

        }
    }
}
