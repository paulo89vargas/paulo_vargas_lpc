﻿namespace aula0903ex1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.capital = new System.Windows.Forms.Label();
            this.txtcapital = new System.Windows.Forms.TextBox();
            this.txtTaxaJuro = new System.Windows.Forms.TextBox();
            this.calcular = new System.Windows.Forms.Button();
            this.txtMontante = new System.Windows.Forms.TextBox();
            this.txtEditar = new System.Windows.Forms.Button();
            this.salvar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNumeroMeses = new System.Windows.Forms.TextBox();
            this.numeromeses = new System.Windows.Forms.Label();
            this.taxaJuros = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioJuroComposto = new System.Windows.Forms.RadioButton();
            this.radioJuroSimples = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.limpa = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // capital
            // 
            this.capital.AutoSize = true;
            this.capital.Location = new System.Drawing.Point(16, 40);
            this.capital.Name = "capital";
            this.capital.Size = new System.Drawing.Size(39, 13);
            this.capital.TabIndex = 3;
            this.capital.Text = "Capital";
            this.capital.Click += new System.EventHandler(this.capital_Click);
            // 
            // txtcapital
            // 
            this.txtcapital.Location = new System.Drawing.Point(61, 38);
            this.txtcapital.Name = "txtcapital";
            this.txtcapital.Size = new System.Drawing.Size(68, 20);
            this.txtcapital.TabIndex = 4;
            this.txtcapital.TextChanged += new System.EventHandler(this.txtcapital_TextChanged);
            // 
            // txtTaxaJuro
            // 
            this.txtTaxaJuro.Location = new System.Drawing.Point(227, 37);
            this.txtTaxaJuro.Name = "txtTaxaJuro";
            this.txtTaxaJuro.Size = new System.Drawing.Size(163, 20);
            this.txtTaxaJuro.TabIndex = 5;
            this.txtTaxaJuro.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // calcular
            // 
            this.calcular.Location = new System.Drawing.Point(577, 282);
            this.calcular.Name = "calcular";
            this.calcular.Size = new System.Drawing.Size(75, 23);
            this.calcular.TabIndex = 20;
            this.calcular.Text = "Calcular";
            this.calcular.Click += new System.EventHandler(this.calcular_Click);
            // 
            // txtMontante
            // 
            this.txtMontante.Location = new System.Drawing.Point(508, 60);
            this.txtMontante.Name = "txtMontante";
            this.txtMontante.Size = new System.Drawing.Size(128, 20);
            this.txtMontante.TabIndex = 13;
            this.txtMontante.TextChanged += new System.EventHandler(this.txtMontante_TextChanged);
            // 
            // txtEditar
            // 
            this.txtEditar.Location = new System.Drawing.Point(299, 282);
            this.txtEditar.Name = "txtEditar";
            this.txtEditar.Size = new System.Drawing.Size(75, 23);
            this.txtEditar.TabIndex = 16;
            this.txtEditar.Text = "Editar";
            this.txtEditar.UseVisualStyleBackColor = true;
            this.txtEditar.Click += new System.EventHandler(this.editar_Click);
            // 
            // salvar
            // 
            this.salvar.Location = new System.Drawing.Point(422, 282);
            this.salvar.Name = "salvar";
            this.salvar.Size = new System.Drawing.Size(75, 23);
            this.salvar.TabIndex = 17;
            this.salvar.Text = "Salvar";
            this.salvar.UseVisualStyleBackColor = true;
            this.salvar.Click += new System.EventHandler(this.salvar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNumeroMeses);
            this.groupBox1.Controls.Add(this.numeromeses);
            this.groupBox1.Controls.Add(this.taxaJuros);
            this.groupBox1.Controls.Add(this.capital);
            this.groupBox1.Controls.Add(this.txtcapital);
            this.groupBox1.Controls.Add(this.txtTaxaJuro);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(676, 99);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtNumeroMeses
            // 
            this.txtNumeroMeses.Location = new System.Drawing.Point(540, 38);
            this.txtNumeroMeses.Name = "txtNumeroMeses";
            this.txtNumeroMeses.Size = new System.Drawing.Size(100, 20);
            this.txtNumeroMeses.TabIndex = 11;
            // 
            // numeromeses
            // 
            this.numeromeses.AutoSize = true;
            this.numeromeses.Location = new System.Drawing.Point(419, 41);
            this.numeromeses.Name = "numeromeses";
            this.numeromeses.Size = new System.Drawing.Size(92, 13);
            this.numeromeses.TabIndex = 10;
            this.numeromeses.Text = "Número de meses";
            // 
            // taxaJuros
            // 
            this.taxaJuros.AutoSize = true;
            this.taxaJuros.Location = new System.Drawing.Point(147, 41);
            this.taxaJuros.Name = "taxaJuros";
            this.taxaJuros.Size = new System.Drawing.Size(74, 13);
            this.taxaJuros.TabIndex = 9;
            this.taxaJuros.Text = "Taxa de Juros";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioJuroComposto);
            this.groupBox2.Controls.Add(this.radioJuroSimples);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.salvar);
            this.groupBox2.Controls.Add(this.txtEditar);
            this.groupBox2.Controls.Add(this.txtMontante);
            this.groupBox2.Location = new System.Drawing.Point(16, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(672, 117);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Taxa de Juros";
            // 
            // radioJuroComposto
            // 
            this.radioJuroComposto.AutoSize = true;
            this.radioJuroComposto.Location = new System.Drawing.Point(210, 60);
            this.radioJuroComposto.Name = "radioJuroComposto";
            this.radioJuroComposto.Size = new System.Drawing.Size(105, 17);
            this.radioJuroComposto.TabIndex = 21;
            this.radioJuroComposto.Text = "Juros Compostos";
            this.radioJuroComposto.UseVisualStyleBackColor = true;
            this.radioJuroComposto.CheckedChanged += new System.EventHandler(this.radioJuroComposto_CheckedChanged);
            // 
            // radioJuroSimples
            // 
            this.radioJuroSimples.AutoSize = true;
            this.radioJuroSimples.Checked = true;
            this.radioJuroSimples.Location = new System.Drawing.Point(33, 63);
            this.radioJuroSimples.Name = "radioJuroSimples";
            this.radioJuroSimples.Size = new System.Drawing.Size(89, 17);
            this.radioJuroSimples.TabIndex = 20;
            this.radioJuroSimples.TabStop = true;
            this.radioJuroSimples.Text = "Juros Simples";
            this.radioJuroSimples.UseVisualStyleBackColor = true;
            this.radioJuroSimples.CheckedChanged += new System.EventHandler(this.radioJuroSimples_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(402, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Montante :";
            // 
            // limpa
            // 
            this.limpa.Location = new System.Drawing.Point(49, 272);
            this.limpa.Name = "limpa";
            this.limpa.Size = new System.Drawing.Size(75, 23);
            this.limpa.TabIndex = 21;
            this.limpa.Text = "Limpar";
            this.limpa.UseVisualStyleBackColor = true;
            this.limpa.Click += new System.EventHandler(this.limpa_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 322);
            this.Controls.Add(this.limpa);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.calcular);
            this.Name = "Form1";
            this.Text = "Calculo de Juros";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label capital;
        private System.Windows.Forms.TextBox txtcapital;
        private System.Windows.Forms.TextBox txtTaxaJuro;
        private System.Windows.Forms.Button calcular;
        private System.Windows.Forms.TextBox txtMontante;
        private System.Windows.Forms.Button txtEditar;
        private System.Windows.Forms.Button salvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label taxaJuros;
        private System.Windows.Forms.TextBox txtNumeroMeses;
        private System.Windows.Forms.Label numeromeses;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioJuroComposto;
        private System.Windows.Forms.RadioButton radioJuroSimples;
        private System.Windows.Forms.Button limpa;
    }
}

