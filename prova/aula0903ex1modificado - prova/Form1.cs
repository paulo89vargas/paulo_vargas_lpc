﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using aula0903ex1.Entidades;
using aula0903ex1.Dao;
namespace aula0903ex1
{
    public partial class Form1 : Form
    {
        private double P;
        private double i;
        private double n;
        private double M;
        
        private PessoaDao pessoaDao;

        public Form1()
        {
            InitializeComponent();
            pessoaDao = new PessoaDao();
            PopularCombobox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void FrmPrincipal_FormClosing(object sender, FormClosedEventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
        private void PopularCombobox()
        {
            //cmbExemplo.DataSource = pessoaDao.GetAll();
            //cmbExemplo.DisplayMember = "Nome";
            //cmbExemplo.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show(cmbExemplo.SelectedValue.ToString());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        //private void pesquisar_Click(object sender, EventArgs e)
        //{
        //    var formulario = new FrmPessoaPesquisa(pessoaDao);
        //    formulario.ShowDialog();
        //}

        private void salvar_Click(object sender, EventArgs e)
        {
            var pessoa = new Pessoa();



            //pessoa.Cpf = int.Parse(txtCPF.Text);
            //pessoa.Rg = Int32.Parse(txtRG.Text);
            //pessoa.Nome = txtNome.Text;
            //pessoa.Idade = int.Parse(txtIdade.Text);
            //pessoa.Semestre = int.Parse(txtSemestre.Text);
            //if (pessoa.Semestre < 3)
            //{ pessoa.VlrInsc = "30"; }
            //if (pessoa.Semestre < 7)
            //{ pessoa.VlrInsc = "20"; }
            //if (pessoa.Semestre >= 7)
            //{ pessoa.VlrInsc = "10"; }

            //txtMontante.Text = pessoa.VlrInsc;
            //pessoaDao.Adicionar(pessoa);
        }

        private void resultdtanasc_TextChanged(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {
            //int cod = int.Parse(txtcapital.Text);

            //var obj = pessoaDao.GetById(cod);

            //if (obj != null)
            //{
                            
            //    txtCPF.Text = obj.Cpf.ToString();
            //    txtRG.Text = obj.Rg.ToString();
            //    txtNome.Text = obj.Nome.ToString();
            //    txtMontante.Text = obj.VlrInsc.ToString();
            //    txtIdade.Text = obj.Idade.ToString();
            //    txtSemestre.Text = obj.Semestre.ToString();
            //}



        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void nomepesquisa_Click(object sender, EventArgs e)
        {

        }

        private void editar_Click(object sender, EventArgs e)
        {
            //var pessoa = new Pessoa();


            
            
            

            //pessoa.Cpf = int.Parse(txtCPF.Text);
            //pessoa.Rg = Int32.Parse(txtRG.Text);
            //pessoa.Nome = txtNome.Text;
            //pessoa.Idade = int.Parse(txtIdade.Text);
            //pessoa.Semestre = int.Parse(txtSemestre.Text);
            //if (pessoa.Semestre < 3)
            //{ pessoa.VlrInsc = "30"; }
            //if (pessoa.Semestre < 7)
            //{ pessoa.VlrInsc = "20"; }
            //if (pessoa.Semestre >= 7)
            //{ pessoa.VlrInsc = "10"; }

            //txtMontante.Text = pessoa.VlrInsc;
            //pessoaDao.Atualizar(pessoa);
        }

       

        

        private void capital_Click(object sender, EventArgs e)
        {

        }

        private void radioJuroSimples_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        
        private void radioJuroComposto_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void txtMontante_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtcapital_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void calcular_Click(object sender, EventArgs e)
        {
            if(txtcapital.Text == "")
            {
                

                MessageBox.Show(
                    "O campo Capital está em branco!",
                    "Alerta!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning

                    );
                return;
            }

            P = double.Parse(txtcapital.Text);

            if (txtTaxaJuro.Text == "")
            {
                

                MessageBox.Show(
                    "O campo Taxa de Juros está em branco!",
                    "Alerta!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning

                    );
                return;
               
            }

            i = double.Parse(txtTaxaJuro.Text);

            if (txtNumeroMeses.Text == "")
            {
                

                MessageBox.Show(
                    "O campo Número de meses está em branco!",
                    "Alerta!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning

                    );

                return;
            }

            n = double.Parse(txtNumeroMeses.Text);




            if (radioJuroSimples.Checked)
                M = P * (1 + ((i/100) * n));

            if(radioJuroComposto.Checked)
                M = P * (Math.Pow(1 + (i/100), n));           

            txtMontante.Text = M.ToString();

        }

        private void limpa_Click(object sender, EventArgs e)
        {
            radioJuroSimples.Checked = true;
            txtcapital.Text = null;
            txtTaxaJuro.Text = null;
            txtNumeroMeses.Text = null;
            txtMontante.Text = null;

        }
    }
}
